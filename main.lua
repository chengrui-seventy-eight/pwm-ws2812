
-- LuaTools需要PROJECT和VERSION这两个信息
PROJECT = "mypwm"
VERSION = "1.0.0"

log.info("main", PROJECT, VERSION)

-- sys库是标配
_G.sys = require("sys")

--添加硬狗防止程序卡死
wdt.init(15000)--初始化watchdog设置为15s
sys.timerLoopStart(wdt.feed, 10000)--10s喂一次狗

-- PWM4 --> PA7-7
-- PWM3 --> PB3-19
-- PWM2 --> PB2-18
-- PWM1 --> PB1-17
-- PWM0 --> PB0-16 
local RGB=zbuff.create({16,16,24},0x010101)
 function clr()
    pwm.open(0, 1000000,1)
    timer.mdelay(220)
    pwm.close(0)
 end
 function update()
    pwm.open(0, 1000000,RGB[0],8)
    pwm.open(0, 1000000,RGB[1],8)
    pwm.open(0, 1000000,RGB[2],8)
    pwm.open(0, 1000000,RGB[3],8)
    pwm.open(0, 1000000,RGB[4],8)
    pwm.open(0, 1000000,RGB[5],8)
    pwm.open(0, 1000000,RGB[6],8)
    pwm.open(0, 1000000,RGB[7],8)
    pwm.open(0, 1000000,RGB[8],8)
    pwm.open(0, 1000000,RGB[9],8)
    pwm.open(0, 1000000,RGB[10],8)

    pwm.open(0, 1000000,RGB[11],8)
    pwm.open(0, 1000000,RGB[12],8)
    pwm.open(0, 1000000,RGB[13],8)
    pwm.open(0, 1000000,RGB[14],8)
    pwm.open(0, 1000000,RGB[15],8)
    pwm.open(0, 1000000,RGB[16],8)
    pwm.open(0, 1000000,RGB[17],8)
    pwm.open(0, 1000000,RGB[18],8)
    pwm.open(0, 1000000,RGB[19],8)
    pwm.open(0, 1000000,RGB[20],8)

    pwm.open(0, 1000000,RGB[21],8)
    pwm.open(0, 1000000,RGB[22],8)
    pwm.open(0, 1000000,RGB[23],8)
    pwm.open(0, 1000000,RGB[24],8)
    pwm.open(0, 1000000,RGB[25],8)
    pwm.open(0, 1000000,RGB[26],8)
    pwm.open(0, 1000000,RGB[27],8)
    pwm.open(0, 1000000,RGB[28],8)
    pwm.open(0, 1000000,RGB[29],8)
    pwm.open(0, 1000000,RGB[30],8)

    pwm.open(0, 1000000,RGB[31],8)
    pwm.open(0, 1000000,RGB[32],8)
    pwm.open(0, 1000000,RGB[33],8)
    pwm.open(0, 1000000,RGB[34],8)
    pwm.open(0, 1000000,RGB[35],8)
    pwm.open(0, 1000000,RGB[36],8)
    pwm.open(0, 1000000,RGB[37],8)
    pwm.open(0, 1000000,RGB[38],8)
    pwm.open(0, 1000000,RGB[39],8)
    pwm.open(0, 1000000,RGB[40],8)

    pwm.open(0, 1000000,RGB[41],8)
    pwm.open(0, 1000000,RGB[42],8)
    pwm.open(0, 1000000,RGB[43],8)
    pwm.open(0, 1000000,RGB[44],8)
    pwm.open(0, 1000000,RGB[45],8)
    pwm.open(0, 1000000,RGB[46],8)
    pwm.open(0, 1000000,RGB[47],8)
    pwm.open(0, 1000000,RGB[48],8)
    pwm.open(0, 1000000,RGB[49],8)
    pwm.open(0, 1000000,RGB[50],8)

    pwm.open(0, 1000000,RGB[51],8)
    pwm.open(0, 1000000,RGB[52],8)
    pwm.open(0, 1000000,RGB[53],8)
    pwm.open(0, 1000000,RGB[54],8)
    pwm.open(0, 1000000,RGB[55],8)
    pwm.open(0, 1000000,RGB[56],8)
    pwm.open(0, 1000000,RGB[57],8)
    pwm.open(0, 1000000,RGB[58],8)
    pwm.open(0, 1000000,RGB[59],8)
   
    pwm.open(0, 1000000,RGB[60],8)
    pwm.open(0, 1000000,RGB[61],8)
    pwm.open(0, 1000000,RGB[62],8)
    pwm.open(0, 1000000,RGB[63],8)
    pwm.open(0, 1000000,RGB[64],8)
    pwm.open(0, 1000000,RGB[65],8)
    pwm.open(0, 1000000,RGB[66],8)
    pwm.open(0, 1000000,RGB[67],8)
    pwm.open(0, 1000000,RGB[68],8)
    pwm.open(0, 1000000,RGB[69],8)
   
    pwm.open(0, 1000000,RGB[70],8)
    pwm.open(0, 1000000,RGB[71],8)
    pwm.open(0, 1000000,RGB[72],8)
    pwm.open(0, 1000000,RGB[73],8)
    pwm.open(0, 1000000,RGB[74],8)
    pwm.open(0, 1000000,RGB[75],8)
    pwm.open(0, 1000000,RGB[76],8)
    pwm.open(0, 1000000,RGB[77],8)
    pwm.open(0, 1000000,RGB[78],8)
    pwm.open(0, 1000000,RGB[79],8)
 
    pwm.open(0, 1000000,RGB[80],8)
    pwm.open(0, 1000000,RGB[81],8)
    pwm.open(0, 1000000,RGB[82],8)
    pwm.open(0, 1000000,RGB[83],8)
    pwm.open(0, 1000000,RGB[84],8)
    pwm.open(0, 1000000,RGB[85],8)
    pwm.open(0, 1000000,RGB[86],8)
    pwm.open(0, 1000000,RGB[87],8)
    pwm.open(0, 1000000,RGB[88],8)
    pwm.open(0, 1000000,RGB[89],8)
  
    pwm.open(0, 1000000,RGB[90],8)
    pwm.open(0, 1000000,RGB[91],8)
    pwm.open(0, 1000000,RGB[92],8)
    pwm.open(0, 1000000,RGB[93],8)
    pwm.open(0, 1000000,RGB[94],8)
    pwm.open(0, 1000000,RGB[95],8)
    pwm.open(0, 1000000,RGB[96],8)
    pwm.open(0, 1000000,RGB[97],8)
    pwm.open(0, 1000000,RGB[98],8)
    pwm.open(0, 1000000,RGB[99],8)
    pwm.open(0, 1000000,RGB[100],8)

    pwm.open(0, 1000000,RGB[101],8)
    pwm.open(0, 1000000,RGB[102],8)
   
    pwm.open(0, 1000000,RGB[103],8)
    pwm.open(0, 1000000,RGB[104],8)
    pwm.open(0, 1000000,RGB[105],8)
    pwm.open(0, 1000000,RGB[106],8)
    pwm.open(0, 1000000,RGB[107],8)
    pwm.open(0, 1000000,RGB[108],8)
    pwm.open(0, 1000000,RGB[109],8)
    pwm.open(0, 1000000,RGB[110],8)

    pwm.open(0, 1000000,RGB[111],8)
    pwm.open(0, 1000000,RGB[112],8)
    pwm.open(0, 1000000,RGB[113],8)
    pwm.open(0, 1000000,RGB[114],8)
    pwm.open(0, 1000000,RGB[115],8)
    pwm.open(0, 1000000,RGB[116],8)
    pwm.open(0, 1000000,RGB[117],8)
    pwm.open(0, 1000000,RGB[118],8)
    pwm.open(0, 1000000,RGB[119],8)
    pwm.open(0, 1000000,RGB[120],8)
    pwm.open(0, 1000000,RGB[121],8)
    pwm.open(0, 1000000,RGB[122],8)
    pwm.open(0, 1000000,RGB[123],8)
    pwm.open(0, 1000000,RGB[124],8)
    pwm.open(0, 1000000,RGB[125],8)
    pwm.open(0, 1000000,RGB[126],8)
    pwm.open(0, 1000000,RGB[127],8)
    pwm.open(0, 1000000,RGB[128],8)
    pwm.open(0, 1000000,RGB[129],8)
    pwm.open(0, 1000000,RGB[130],8)

    pwm.open(0, 1000000,RGB[131],8)
    pwm.open(0, 1000000,RGB[132],8)
    pwm.open(0, 1000000,RGB[133],8)
    pwm.open(0, 1000000,RGB[134],8)
    pwm.open(0, 1000000,RGB[135],8)
    pwm.open(0, 1000000,RGB[136],8)
    pwm.open(0, 1000000,RGB[137],8)
    pwm.open(0, 1000000,RGB[138],8)
    pwm.open(0, 1000000,RGB[139],8)
    pwm.open(0, 1000000,RGB[140],8)

    pwm.open(0, 1000000,RGB[141],8)
    pwm.open(0, 1000000,RGB[142],8)
    pwm.open(0, 1000000,RGB[143],8)
    pwm.open(0, 1000000,RGB[144],8)
    pwm.open(0, 1000000,RGB[145],8)
    pwm.open(0, 1000000,RGB[146],8)
    pwm.open(0, 1000000,RGB[147],8)
    pwm.open(0, 1000000,RGB[148],8)
    pwm.open(0, 1000000,RGB[149],8)
    pwm.open(0, 1000000,RGB[150],8)

    pwm.open(0, 1000000,RGB[151],8)
    pwm.open(0, 1000000,RGB[152],8)
    pwm.open(0, 1000000,RGB[153],8)
    pwm.open(0, 1000000,RGB[154],8)
    pwm.open(0, 1000000,RGB[155],8)
    pwm.open(0, 1000000,RGB[156],8)
    pwm.open(0, 1000000,RGB[157],8)
    pwm.open(0, 1000000,RGB[158],8)
    pwm.open(0, 1000000,RGB[159],8)
   
    pwm.open(0, 1000000,RGB[160],8)
    pwm.open(0, 1000000,RGB[161],8)
    pwm.open(0, 1000000,RGB[162],8)
    pwm.open(0, 1000000,RGB[163],8)
    pwm.open(0, 1000000,RGB[164],8)
    pwm.open(0, 1000000,RGB[165],8)
    pwm.open(0, 1000000,RGB[166],8)
    pwm.open(0, 1000000,RGB[167],8)
    pwm.open(0, 1000000,RGB[168],8)
    pwm.open(0, 1000000,RGB[169],8)
   
    pwm.open(0, 1000000,RGB[170],8)
    pwm.open(0, 1000000,RGB[171],8)
    pwm.open(0, 1000000,RGB[172],8)
    pwm.open(0, 1000000,RGB[173],8)
    pwm.open(0, 1000000,RGB[174],8)
    pwm.open(0, 1000000,RGB[175],8)
    pwm.open(0, 1000000,RGB[176],8)
    pwm.open(0, 1000000,RGB[177],8)
    pwm.open(0, 1000000,RGB[178],8)
    pwm.open(0, 1000000,RGB[179],8)
 
    pwm.open(0, 1000000,RGB[180],8)
    pwm.open(0, 1000000,RGB[181],8)
    pwm.open(0, 1000000,RGB[182],8)
    pwm.open(0, 1000000,RGB[183],8)
    pwm.open(0, 1000000,RGB[184],8)
    pwm.open(0, 1000000,RGB[185],8)
    pwm.open(0, 1000000,RGB[186],8)
    pwm.open(0, 1000000,RGB[187],8)
    pwm.open(0, 1000000,RGB[188],8)
    pwm.open(0, 1000000,RGB[189],8)
  
    pwm.open(0, 1000000,RGB[190],8)
    pwm.open(0, 1000000,RGB[191],8)
    pwm.open(0, 1000000,RGB[192],8)
    pwm.open(0, 1000000,RGB[193],8)
    pwm.open(0, 1000000,RGB[194],8)
    pwm.open(0, 1000000,RGB[195],8)
    pwm.open(0, 1000000,RGB[196],8)
    pwm.open(0, 1000000,RGB[197],8)
    pwm.open(0, 1000000,RGB[198],8)
    pwm.open(0, 1000000,RGB[199],8)
    pwm.open(0, 1000000,RGB[200],8)
    pwm.open(0, 1000000,RGB[201],8)
    pwm.open(0, 1000000,RGB[202],8)
   
    pwm.open(0, 1000000,RGB[203],8)
    pwm.open(0, 1000000,RGB[204],8)
    pwm.open(0, 1000000,RGB[205],8)
    pwm.open(0, 1000000,RGB[206],8)
    pwm.open(0, 1000000,RGB[207],8)
    pwm.open(0, 1000000,RGB[208],8)
    pwm.open(0, 1000000,RGB[209],8)
    pwm.open(0, 1000000,RGB[210],8)

    pwm.open(0, 1000000,RGB[211],8)
    pwm.open(0, 1000000,RGB[212],8)
    pwm.open(0, 1000000,RGB[213],8)
    pwm.open(0, 1000000,RGB[214],8)
    pwm.open(0, 1000000,RGB[215],8)
    pwm.open(0, 1000000,RGB[216],8)
    pwm.open(0, 1000000,RGB[217],8)
    pwm.open(0, 1000000,RGB[218],8)
    pwm.open(0, 1000000,RGB[219],8)
    pwm.open(0, 1000000,RGB[220],8)
    pwm.open(0, 1000000,RGB[221],8)
    pwm.open(0, 1000000,RGB[222],8)
    pwm.open(0, 1000000,RGB[223],8)
    pwm.open(0, 1000000,RGB[224],8)
    pwm.open(0, 1000000,RGB[225],8)
    pwm.open(0, 1000000,RGB[226],8)
    pwm.open(0, 1000000,RGB[227],8)
    pwm.open(0, 1000000,RGB[228],8)
    pwm.open(0, 1000000,RGB[229],8)
    pwm.open(0, 1000000,RGB[230],8)

    pwm.open(0, 1000000,RGB[231],8)
    pwm.open(0, 1000000,RGB[232],8)
    pwm.open(0, 1000000,RGB[233],8)
    pwm.open(0, 1000000,RGB[234],8)
    pwm.open(0, 1000000,RGB[235],8)
    pwm.open(0, 1000000,RGB[236],8)
    pwm.open(0, 1000000,RGB[237],8)
    pwm.open(0, 1000000,RGB[238],8)
    pwm.open(0, 1000000,RGB[239],8)
    pwm.open(0, 1000000,RGB[240],8)

    pwm.open(0, 1000000,RGB[241],8)
    pwm.open(0, 1000000,RGB[242],8)
    pwm.open(0, 1000000,RGB[243],8)
    pwm.open(0, 1000000,RGB[244],8)
    pwm.open(0, 1000000,RGB[245],8)
    pwm.open(0, 1000000,RGB[246],8)
    pwm.open(0, 1000000,RGB[247],8)
    pwm.open(0, 1000000,RGB[248],8)
    pwm.open(0, 1000000,RGB[249],8)
    pwm.open(0, 1000000,RGB[250],8)

    pwm.open(0, 1000000,RGB[251],8)
    pwm.open(0, 1000000,RGB[252],8)
    pwm.open(0, 1000000,RGB[253],8)
    pwm.open(0, 1000000,RGB[254],8)
    pwm.open(0, 1000000,RGB[255],8)
    pwm.open(0, 1000000,RGB[256],8)
    pwm.open(0, 1000000,RGB[257],8)
    pwm.open(0, 1000000,RGB[258],8)
    pwm.open(0, 1000000,RGB[259],8)
   
    pwm.open(0, 1000000,RGB[260],8)
    pwm.open(0, 1000000,RGB[261],8)
    pwm.open(0, 1000000,RGB[262],8)
    pwm.open(0, 1000000,RGB[263],8)
    pwm.open(0, 1000000,RGB[264],8)
    pwm.open(0, 1000000,RGB[265],8)
    pwm.open(0, 1000000,RGB[266],8)
    pwm.open(0, 1000000,RGB[267],8)
    pwm.open(0, 1000000,RGB[268],8)
    pwm.open(0, 1000000,RGB[269],8)
   
    pwm.open(0, 1000000,RGB[270],8)
    pwm.open(0, 1000000,RGB[271],8)
    pwm.open(0, 1000000,RGB[272],8)
    pwm.open(0, 1000000,RGB[273],8)
    pwm.open(0, 1000000,RGB[274],8)
    pwm.open(0, 1000000,RGB[275],8)
    pwm.open(0, 1000000,RGB[276],8)
    pwm.open(0, 1000000,RGB[277],8)
    pwm.open(0, 1000000,RGB[278],8)
    pwm.open(0, 1000000,RGB[279],8)
 
    pwm.open(0, 1000000,RGB[280],8)
    pwm.open(0, 1000000,RGB[281],8)
    pwm.open(0, 1000000,RGB[282],8)
    pwm.open(0, 1000000,RGB[283],8)
    pwm.open(0, 1000000,RGB[284],8)
    pwm.open(0, 1000000,RGB[285],8)
    pwm.open(0, 1000000,RGB[286],8)
    pwm.open(0, 1000000,RGB[287],8)
    pwm.open(0, 1000000,RGB[288],8)
    pwm.open(0, 1000000,RGB[289],8)
  
    pwm.open(0, 1000000,RGB[290],8)
    pwm.open(0, 1000000,RGB[291],8)
    pwm.open(0, 1000000,RGB[292],8)
    pwm.open(0, 1000000,RGB[293],8)
    pwm.open(0, 1000000,RGB[294],8)
    pwm.open(0, 1000000,RGB[295],8)
    pwm.open(0, 1000000,RGB[296],8)
    pwm.open(0, 1000000,RGB[297],8)
    pwm.open(0, 1000000,RGB[298],8)
    pwm.open(0, 1000000,RGB[299],8)
    
    pwm.open(0, 1000000,RGB[300],8)
    pwm.open(0, 1000000,RGB[301],8)
    pwm.open(0, 1000000,RGB[302],8)
   
    pwm.open(0, 1000000,RGB[303],8)
    pwm.open(0, 1000000,RGB[304],8)
    pwm.open(0, 1000000,RGB[305],8)
    pwm.open(0, 1000000,RGB[306],8)
    pwm.open(0, 1000000,RGB[307],8)
    pwm.open(0, 1000000,RGB[308],8)
    pwm.open(0, 1000000,RGB[309],8)
    pwm.open(0, 1000000,RGB[310],8)

    pwm.open(0, 1000000,RGB[311],8)
    pwm.open(0, 1000000,RGB[312],8)
    pwm.open(0, 1000000,RGB[313],8)
    pwm.open(0, 1000000,RGB[314],8)
    pwm.open(0, 1000000,RGB[315],8)
    pwm.open(0, 1000000,RGB[316],8)
    pwm.open(0, 1000000,RGB[317],8)
    pwm.open(0, 1000000,RGB[318],8)
    pwm.open(0, 1000000,RGB[319],8)
    pwm.open(0, 1000000,RGB[320],8)
    pwm.open(0, 1000000,RGB[321],8)
    pwm.open(0, 1000000,RGB[322],8)
    pwm.open(0, 1000000,RGB[323],8)
    pwm.open(0, 1000000,RGB[324],8)
    pwm.open(0, 1000000,RGB[325],8)
    pwm.open(0, 1000000,RGB[326],8)
    pwm.open(0, 1000000,RGB[327],8)
    pwm.open(0, 1000000,RGB[328],8)
    pwm.open(0, 1000000,RGB[329],8)
    pwm.open(0, 1000000,RGB[330],8)

    pwm.open(0, 1000000,RGB[331],8)
    pwm.open(0, 1000000,RGB[332],8)
    pwm.open(0, 1000000,RGB[333],8)
    pwm.open(0, 1000000,RGB[334],8)
    pwm.open(0, 1000000,RGB[335],8)
    pwm.open(0, 1000000,RGB[336],8)
    pwm.open(0, 1000000,RGB[337],8)
    pwm.open(0, 1000000,RGB[338],8)
    pwm.open(0, 1000000,RGB[339],8)
    pwm.open(0, 1000000,RGB[340],8)

    pwm.open(0, 1000000,RGB[341],8)
    pwm.open(0, 1000000,RGB[342],8)
    pwm.open(0, 1000000,RGB[343],8)
    pwm.open(0, 1000000,RGB[344],8)
    pwm.open(0, 1000000,RGB[345],8)
    pwm.open(0, 1000000,RGB[346],8)
    pwm.open(0, 1000000,RGB[347],8)
    pwm.open(0, 1000000,RGB[348],8)
    pwm.open(0, 1000000,RGB[349],8)
    pwm.open(0, 1000000,RGB[350],8)

    pwm.open(0, 1000000,RGB[351],8)
    pwm.open(0, 1000000,RGB[352],8)
    pwm.open(0, 1000000,RGB[353],8)
    pwm.open(0, 1000000,RGB[354],8)
    pwm.open(0, 1000000,RGB[355],8)
    pwm.open(0, 1000000,RGB[356],8)
    pwm.open(0, 1000000,RGB[357],8)
    pwm.open(0, 1000000,RGB[358],8)
    pwm.open(0, 1000000,RGB[359],8)
   
    pwm.open(0, 1000000,RGB[360],8)
    pwm.open(0, 1000000,RGB[361],8)
    pwm.open(0, 1000000,RGB[362],8)
    pwm.open(0, 1000000,RGB[363],8)
    pwm.open(0, 1000000,RGB[364],8)
    pwm.open(0, 1000000,RGB[365],8)
    pwm.open(0, 1000000,RGB[366],8)
    pwm.open(0, 1000000,RGB[367],8)
    pwm.open(0, 1000000,RGB[368],8)
    pwm.open(0, 1000000,RGB[369],8)

    pwm.open(0, 1000000,RGB[370],8)
    pwm.open(0, 1000000,RGB[371],8)
    pwm.open(0, 1000000,RGB[372],8)
    pwm.open(0, 1000000,RGB[373],8)
    pwm.open(0, 1000000,RGB[374],8)
    pwm.open(0, 1000000,RGB[375],8)
    pwm.open(0, 1000000,RGB[376],8)
    pwm.open(0, 1000000,RGB[377],8)
    pwm.open(0, 1000000,RGB[378],8)
    pwm.open(0, 1000000,RGB[379],8)
 
    pwm.open(0, 1000000,RGB[380],8)
    pwm.open(0, 1000000,RGB[381],8)
    pwm.open(0, 1000000,RGB[382],8)
    pwm.open(0, 1000000,RGB[383],8)
    pwm.open(0, 1000000,RGB[384],8)
    pwm.open(0, 1000000,RGB[385],8)
    pwm.open(0, 1000000,RGB[386],8)
    pwm.open(0, 1000000,RGB[387],8)
    pwm.open(0, 1000000,RGB[388],8)
    pwm.open(0, 1000000,RGB[389],8)
  
    pwm.open(0, 1000000,RGB[390],8)
    pwm.open(0, 1000000,RGB[391],8)
    pwm.open(0, 1000000,RGB[392],8)
    pwm.open(0, 1000000,RGB[393],8)
    pwm.open(0, 1000000,RGB[394],8)
    pwm.open(0, 1000000,RGB[395],8)
    pwm.open(0, 1000000,RGB[396],8)
    pwm.open(0, 1000000,RGB[397],8)
    pwm.open(0, 1000000,RGB[398],8)
    pwm.open(0, 1000000,RGB[399],8)
   
    pwm.open(0, 1000000,RGB[400],8)
    pwm.open(0, 1000000,RGB[401],8)
    pwm.open(0, 1000000,RGB[402],8)

    pwm.open(0, 1000000,RGB[403],8)
    pwm.open(0, 1000000,RGB[404],8)
    pwm.open(0, 1000000,RGB[405],8)
    pwm.open(0, 1000000,RGB[406],8)
    pwm.open(0, 1000000,RGB[407],8)
    pwm.open(0, 1000000,RGB[408],8)
    pwm.open(0, 1000000,RGB[409],8)
    pwm.open(0, 1000000,RGB[410],8)

    pwm.open(0, 1000000,RGB[411],8)
    pwm.open(0, 1000000,RGB[412],8)
    pwm.open(0, 1000000,RGB[413],8)
    pwm.open(0, 1000000,RGB[414],8)
    pwm.open(0, 1000000,RGB[415],8)
    pwm.open(0, 1000000,RGB[416],8)
    pwm.open(0, 1000000,RGB[417],8)
    pwm.open(0, 1000000,RGB[418],8)
    pwm.open(0, 1000000,RGB[419],8)
    pwm.open(0, 1000000,RGB[420],8)
    pwm.open(0, 1000000,RGB[421],8)
    pwm.open(0, 1000000,RGB[422],8)
    pwm.open(0, 1000000,RGB[423],8)
    pwm.open(0, 1000000,RGB[424],8)
    pwm.open(0, 1000000,RGB[425],8)
    pwm.open(0, 1000000,RGB[426],8)
    pwm.open(0, 1000000,RGB[427],8)
    pwm.open(0, 1000000,RGB[428],8)
    pwm.open(0, 1000000,RGB[429],8)
    pwm.open(0, 1000000,RGB[430],8)

    pwm.open(0, 1000000,RGB[431],8)
    pwm.open(0, 1000000,RGB[432],8)
    pwm.open(0, 1000000,RGB[433],8)
    pwm.open(0, 1000000,RGB[434],8)
    pwm.open(0, 1000000,RGB[435],8)
    pwm.open(0, 1000000,RGB[436],8)
    pwm.open(0, 1000000,RGB[437],8)
    pwm.open(0, 1000000,RGB[438],8)
    pwm.open(0, 1000000,RGB[439],8)
    pwm.open(0, 1000000,RGB[440],8)

    pwm.open(0, 1000000,RGB[441],8)
    pwm.open(0, 1000000,RGB[442],8)
    pwm.open(0, 1000000,RGB[443],8)
    pwm.open(0, 1000000,RGB[444],8)
    pwm.open(0, 1000000,RGB[445],8)
    pwm.open(0, 1000000,RGB[446],8)
    pwm.open(0, 1000000,RGB[447],8)
    pwm.open(0, 1000000,RGB[448],8)
    pwm.open(0, 1000000,RGB[449],8)
    pwm.open(0, 1000000,RGB[450],8)

    pwm.open(0, 1000000,RGB[451],8)
    pwm.open(0, 1000000,RGB[452],8)
    pwm.open(0, 1000000,RGB[453],8)
    pwm.open(0, 1000000,RGB[454],8)
    pwm.open(0, 1000000,RGB[455],8)
    pwm.open(0, 1000000,RGB[456],8)
    pwm.open(0, 1000000,RGB[457],8)
    pwm.open(0, 1000000,RGB[458],8)
    pwm.open(0, 1000000,RGB[459],8)
   
    pwm.open(0, 1000000,RGB[460],8)
    pwm.open(0, 1000000,RGB[461],8)
    pwm.open(0, 1000000,RGB[462],8)
    pwm.open(0, 1000000,RGB[463],8)
    pwm.open(0, 1000000,RGB[464],8)
    pwm.open(0, 1000000,RGB[465],8)
    pwm.open(0, 1000000,RGB[466],8)
    pwm.open(0, 1000000,RGB[467],8)
    pwm.open(0, 1000000,RGB[468],8)
    pwm.open(0, 1000000,RGB[469],8)
   
    pwm.open(0, 1000000,RGB[470],8)
    pwm.open(0, 1000000,RGB[471],8)
    pwm.open(0, 1000000,RGB[472],8)
    pwm.open(0, 1000000,RGB[473],8)
    pwm.open(0, 1000000,RGB[474],8)
    pwm.open(0, 1000000,RGB[475],8)
    pwm.open(0, 1000000,RGB[476],8)
    pwm.open(0, 1000000,RGB[477],8)
    pwm.open(0, 1000000,RGB[478],8)
    pwm.open(0, 1000000,RGB[479],8)
 
    pwm.open(0, 1000000,RGB[480],8)
    pwm.open(0, 1000000,RGB[481],8)
    pwm.open(0, 1000000,RGB[482],8)
    pwm.open(0, 1000000,RGB[483],8)
    pwm.open(0, 1000000,RGB[484],8)
    pwm.open(0, 1000000,RGB[485],8)
    pwm.open(0, 1000000,RGB[486],8)
    pwm.open(0, 1000000,RGB[487],8)
    pwm.open(0, 1000000,RGB[488],8)
    pwm.open(0, 1000000,RGB[489],8)
  
    pwm.open(0, 1000000,RGB[490],8)
    pwm.open(0, 1000000,RGB[491],8)
    pwm.open(0, 1000000,RGB[492],8)
    pwm.open(0, 1000000,RGB[493],8)
    pwm.open(0, 1000000,RGB[494],8)
    pwm.open(0, 1000000,RGB[495],8)
    pwm.open(0, 1000000,RGB[496],8)
    pwm.open(0, 1000000,RGB[497],8)
    pwm.open(0, 1000000,RGB[498],8)
    pwm.open(0, 1000000,RGB[499],8)
   
    pwm.open(0, 1000000,RGB[500],8)
    pwm.open(0, 1000000,RGB[501],8)
    pwm.open(0, 1000000,RGB[502],8)
   
    pwm.open(0, 1000000,RGB[503],8)
    pwm.open(0, 1000000,RGB[504],8)
    pwm.open(0, 1000000,RGB[505],8)
    pwm.open(0, 1000000,RGB[506],8)
    pwm.open(0, 1000000,RGB[507],8)
    pwm.open(0, 1000000,RGB[508],8)
    pwm.open(0, 1000000,RGB[509],8)
    pwm.open(0, 1000000,RGB[510],8)

    pwm.open(0, 1000000,RGB[511],8)
    pwm.open(0, 1000000,RGB[512],8)
    pwm.open(0, 1000000,RGB[513],8)
    pwm.open(0, 1000000,RGB[514],8)
    pwm.open(0, 1000000,RGB[515],8)
    pwm.open(0, 1000000,RGB[516],8)
    pwm.open(0, 1000000,RGB[517],8)
    pwm.open(0, 1000000,RGB[518],8)
    pwm.open(0, 1000000,RGB[519],8)
    pwm.open(0, 1000000,RGB[520],8)
    pwm.open(0, 1000000,RGB[521],8)
    pwm.open(0, 1000000,RGB[522],8)
    pwm.open(0, 1000000,RGB[523],8)
    pwm.open(0, 1000000,RGB[524],8)
    pwm.open(0, 1000000,RGB[525],8)
    pwm.open(0, 1000000,RGB[526],8)
    pwm.open(0, 1000000,RGB[527],8)
    pwm.open(0, 1000000,RGB[528],8)
    pwm.open(0, 1000000,RGB[529],8)
    pwm.open(0, 1000000,RGB[530],8)

    pwm.open(0, 1000000,RGB[531],8)
    pwm.open(0, 1000000,RGB[532],8)
    pwm.open(0, 1000000,RGB[533],8)
    pwm.open(0, 1000000,RGB[534],8)
    pwm.open(0, 1000000,RGB[535],8)
    pwm.open(0, 1000000,RGB[536],8)
    pwm.open(0, 1000000,RGB[537],8)
    pwm.open(0, 1000000,RGB[538],8)
    pwm.open(0, 1000000,RGB[539],8)
    pwm.open(0, 1000000,RGB[540],8)

    pwm.open(0, 1000000,RGB[541],8)
    pwm.open(0, 1000000,RGB[542],8)
    pwm.open(0, 1000000,RGB[543],8)
    pwm.open(0, 1000000,RGB[544],8)
    pwm.open(0, 1000000,RGB[545],8)
    pwm.open(0, 1000000,RGB[546],8)
    pwm.open(0, 1000000,RGB[547],8)
    pwm.open(0, 1000000,RGB[548],8)
    pwm.open(0, 1000000,RGB[549],8)
    pwm.open(0, 1000000,RGB[550],8)

    pwm.open(0, 1000000,RGB[551],8)
    pwm.open(0, 1000000,RGB[552],8)
    pwm.open(0, 1000000,RGB[553],8)
    pwm.open(0, 1000000,RGB[554],8)
    pwm.open(0, 1000000,RGB[555],8)
    pwm.open(0, 1000000,RGB[556],8)
    pwm.open(0, 1000000,RGB[557],8)
    pwm.open(0, 1000000,RGB[558],8)
    pwm.open(0, 1000000,RGB[559],8)
   
    pwm.open(0, 1000000,RGB[560],8)
    pwm.open(0, 1000000,RGB[561],8)
    pwm.open(0, 1000000,RGB[562],8)
    pwm.open(0, 1000000,RGB[563],8)
    pwm.open(0, 1000000,RGB[564],8)
    pwm.open(0, 1000000,RGB[565],8)
    pwm.open(0, 1000000,RGB[566],8)
    pwm.open(0, 1000000,RGB[567],8)
    pwm.open(0, 1000000,RGB[568],8)
    pwm.open(0, 1000000,RGB[569],8)
   
    pwm.open(0, 1000000,RGB[570],8)
    pwm.open(0, 1000000,RGB[571],8)
    pwm.open(0, 1000000,RGB[572],8)
    pwm.open(0, 1000000,RGB[573],8)
    pwm.open(0, 1000000,RGB[574],8)
    pwm.open(0, 1000000,RGB[575],8)
    pwm.open(0, 1000000,RGB[576],8)
    pwm.open(0, 1000000,RGB[577],8)
    pwm.open(0, 1000000,RGB[578],8)
    pwm.open(0, 1000000,RGB[579],8)
 
    pwm.open(0, 1000000,RGB[580],8)
    pwm.open(0, 1000000,RGB[581],8)
    pwm.open(0, 1000000,RGB[582],8)
    pwm.open(0, 1000000,RGB[583],8)
    pwm.open(0, 1000000,RGB[584],8)
    pwm.open(0, 1000000,RGB[585],8)
    pwm.open(0, 1000000,RGB[586],8)
    pwm.open(0, 1000000,RGB[587],8)
    pwm.open(0, 1000000,RGB[588],8)
    pwm.open(0, 1000000,RGB[589],8)
  
    pwm.open(0, 1000000,RGB[590],8)
    pwm.open(0, 1000000,RGB[591],8)
    pwm.open(0, 1000000,RGB[592],8)
    pwm.open(0, 1000000,RGB[593],8)
    pwm.open(0, 1000000,RGB[594],8)
    pwm.open(0, 1000000,RGB[595],8)
    pwm.open(0, 1000000,RGB[596],8)
    pwm.open(0, 1000000,RGB[597],8)
    pwm.open(0, 1000000,RGB[598],8)
    pwm.open(0, 1000000,RGB[599],8)
   
    pwm.open(0, 1000000,RGB[600],8)
    pwm.open(0, 1000000,RGB[601],8)
    pwm.open(0, 1000000,RGB[602],8)
   
    pwm.open(0, 1000000,RGB[603],8)
    pwm.open(0, 1000000,RGB[604],8)
    pwm.open(0, 1000000,RGB[605],8)
    pwm.open(0, 1000000,RGB[606],8)
    pwm.open(0, 1000000,RGB[607],8)
    pwm.open(0, 1000000,RGB[608],8)
    pwm.open(0, 1000000,RGB[609],8)
    pwm.open(0, 1000000,RGB[610],8)

    pwm.open(0, 1000000,RGB[611],8)
    pwm.open(0, 1000000,RGB[612],8)
    pwm.open(0, 1000000,RGB[613],8)
    pwm.open(0, 1000000,RGB[614],8)
    pwm.open(0, 1000000,RGB[615],8)
    pwm.open(0, 1000000,RGB[616],8)
    pwm.open(0, 1000000,RGB[617],8)
    pwm.open(0, 1000000,RGB[618],8)
    pwm.open(0, 1000000,RGB[619],8)
    pwm.open(0, 1000000,RGB[620],8)
    pwm.open(0, 1000000,RGB[621],8)
    pwm.open(0, 1000000,RGB[622],8)
    pwm.open(0, 1000000,RGB[623],8)
    pwm.open(0, 1000000,RGB[624],8)
    pwm.open(0, 1000000,RGB[625],8)
    pwm.open(0, 1000000,RGB[626],8)
    pwm.open(0, 1000000,RGB[627],8)
    pwm.open(0, 1000000,RGB[628],8)
    pwm.open(0, 1000000,RGB[629],8)
    pwm.open(0, 1000000,RGB[630],8)

    pwm.open(0, 1000000,RGB[631],8)
    pwm.open(0, 1000000,RGB[632],8)
    pwm.open(0, 1000000,RGB[633],8)
    pwm.open(0, 1000000,RGB[634],8)
    pwm.open(0, 1000000,RGB[635],8)
    pwm.open(0, 1000000,RGB[636],8)
    pwm.open(0, 1000000,RGB[637],8)
    pwm.open(0, 1000000,RGB[638],8)
    pwm.open(0, 1000000,RGB[639],8)
    pwm.open(0, 1000000,RGB[640],8)

    pwm.open(0, 1000000,RGB[641],8)
    pwm.open(0, 1000000,RGB[642],8)
    pwm.open(0, 1000000,RGB[643],8)
    pwm.open(0, 1000000,RGB[644],8)
    pwm.open(0, 1000000,RGB[645],8)
    pwm.open(0, 1000000,RGB[646],8)
    pwm.open(0, 1000000,RGB[647],8)
    pwm.open(0, 1000000,RGB[648],8)
    pwm.open(0, 1000000,RGB[649],8)
    pwm.open(0, 1000000,RGB[650],8)

    pwm.open(0, 1000000,RGB[651],8)
    pwm.open(0, 1000000,RGB[652],8)
    pwm.open(0, 1000000,RGB[653],8)
    pwm.open(0, 1000000,RGB[654],8)
    pwm.open(0, 1000000,RGB[655],8)
    pwm.open(0, 1000000,RGB[656],8)
    pwm.open(0, 1000000,RGB[657],8)
    pwm.open(0, 1000000,RGB[658],8)
    pwm.open(0, 1000000,RGB[659],8)
   
    pwm.open(0, 1000000,RGB[660],8)
    pwm.open(0, 1000000,RGB[661],8)
    pwm.open(0, 1000000,RGB[662],8)
    pwm.open(0, 1000000,RGB[663],8)
    pwm.open(0, 1000000,RGB[664],8)
    pwm.open(0, 1000000,RGB[665],8)
    pwm.open(0, 1000000,RGB[666],8)
    pwm.open(0, 1000000,RGB[667],8)
    pwm.open(0, 1000000,RGB[668],8)
    pwm.open(0, 1000000,RGB[669],8)
   
    pwm.open(0, 1000000,RGB[670],8)
    pwm.open(0, 1000000,RGB[671],8)
    pwm.open(0, 1000000,RGB[672],8)
    pwm.open(0, 1000000,RGB[673],8)
    pwm.open(0, 1000000,RGB[674],8)
    pwm.open(0, 1000000,RGB[675],8)
    pwm.open(0, 1000000,RGB[676],8)
    pwm.open(0, 1000000,RGB[677],8)
    pwm.open(0, 1000000,RGB[678],8)
    pwm.open(0, 1000000,RGB[679],8)
 
    pwm.open(0, 1000000,RGB[680],8)
    pwm.open(0, 1000000,RGB[681],8)
    pwm.open(0, 1000000,RGB[682],8)
    pwm.open(0, 1000000,RGB[683],8)
    pwm.open(0, 1000000,RGB[684],8)
    pwm.open(0, 1000000,RGB[685],8)
    pwm.open(0, 1000000,RGB[686],8)
    pwm.open(0, 1000000,RGB[687],8)
    pwm.open(0, 1000000,RGB[688],8)
    pwm.open(0, 1000000,RGB[689],8)
  
    pwm.open(0, 1000000,RGB[690],8)
    pwm.open(0, 1000000,RGB[691],8)
    pwm.open(0, 1000000,RGB[692],8)
    pwm.open(0, 1000000,RGB[693],8)
    pwm.open(0, 1000000,RGB[694],8)
    pwm.open(0, 1000000,RGB[695],8)
    pwm.open(0, 1000000,RGB[696],8)
    pwm.open(0, 1000000,RGB[697],8)
    pwm.open(0, 1000000,RGB[698],8)
    pwm.open(0, 1000000,RGB[699],8)
    pwm.open(0, 1000000,RGB[700],8)
    pwm.open(0, 1000000,RGB[701],8)
    pwm.open(0, 1000000,RGB[702],8)
   
    pwm.open(0, 1000000,RGB[703],8)
    pwm.open(0, 1000000,RGB[704],8)
    pwm.open(0, 1000000,RGB[705],8)
    pwm.open(0, 1000000,RGB[706],8)
    pwm.open(0, 1000000,RGB[707],8)
    pwm.open(0, 1000000,RGB[708],8)
    pwm.open(0, 1000000,RGB[709],8)
    pwm.open(0, 1000000,RGB[710],8)

    pwm.open(0, 1000000,RGB[711],8)
    pwm.open(0, 1000000,RGB[712],8)
    pwm.open(0, 1000000,RGB[713],8)
    pwm.open(0, 1000000,RGB[714],8)
    pwm.open(0, 1000000,RGB[715],8)
    pwm.open(0, 1000000,RGB[716],8)
    pwm.open(0, 1000000,RGB[717],8)
    pwm.open(0, 1000000,RGB[718],8)
    pwm.open(0, 1000000,RGB[719],8)
    pwm.open(0, 1000000,RGB[720],8)
    pwm.open(0, 1000000,RGB[721],8)
    pwm.open(0, 1000000,RGB[722],8)
    pwm.open(0, 1000000,RGB[723],8)
    pwm.open(0, 1000000,RGB[724],8)
    pwm.open(0, 1000000,RGB[725],8)
    pwm.open(0, 1000000,RGB[726],8)
    pwm.open(0, 1000000,RGB[727],8)
    pwm.open(0, 1000000,RGB[728],8)
    pwm.open(0, 1000000,RGB[729],8)
    pwm.open(0, 1000000,RGB[730],8)

    pwm.open(0, 1000000,RGB[731],8)
    pwm.open(0, 1000000,RGB[732],8)
    pwm.open(0, 1000000,RGB[733],8)
    pwm.open(0, 1000000,RGB[734],8)
    pwm.open(0, 1000000,RGB[735],8)
    pwm.open(0, 1000000,RGB[736],8)
    pwm.open(0, 1000000,RGB[737],8)
    pwm.open(0, 1000000,RGB[738],8)
    pwm.open(0, 1000000,RGB[739],8)
    pwm.open(0, 1000000,RGB[740],8)

    pwm.open(0, 1000000,RGB[741],8)
    pwm.open(0, 1000000,RGB[742],8)
    pwm.open(0, 1000000,RGB[743],8)
    pwm.open(0, 1000000,RGB[744],8)
    pwm.open(0, 1000000,RGB[745],8)
    pwm.open(0, 1000000,RGB[746],8)
    pwm.open(0, 1000000,RGB[747],8)
    pwm.open(0, 1000000,RGB[748],8)
    pwm.open(0, 1000000,RGB[749],8)
    pwm.open(0, 1000000,RGB[750],8)

    pwm.open(0, 1000000,RGB[751],8)
    pwm.open(0, 1000000,RGB[752],8)
    pwm.open(0, 1000000,RGB[753],8)
    pwm.open(0, 1000000,RGB[754],8)
    pwm.open(0, 1000000,RGB[755],8)
    pwm.open(0, 1000000,RGB[756],8)
    pwm.open(0, 1000000,RGB[757],8)
    pwm.open(0, 1000000,RGB[758],8)
    pwm.open(0, 1000000,RGB[759],8)
   
    pwm.open(0, 1000000,RGB[760],8)
    pwm.open(0, 1000000,RGB[761],8)
    pwm.open(0, 1000000,RGB[762],8)
    pwm.open(0, 1000000,RGB[763],8)
    pwm.open(0, 1000000,RGB[764],8)
    pwm.open(0, 1000000,RGB[765],8)
    pwm.open(0, 1000000,RGB[766],8)
    pwm.open(0, 1000000,RGB[767],8)
 end
  local lib_5X7={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x5F,0x00,0x00,0x00,0x07,0x00,0x07,0x00,0x14,0x7F,0x14,0x7F,0x14,0x24,0x2A,0x07,0x2A,0x12,0x23,0x13,0x08,0x64,0x62,0x37,0x49,0x55,0x22,0x50,0x00,0x05,0x03,0x00,0x00,0x00,0x1C,0x22,0x41,0x00,0x00,0x41,0x22,0x1C,0x00,0x08,0x2A,0x1C,0x2A,0x08,0x08,0x08,0x3E,0x08,0x08,0x00,0x50,0x30,0x00,0x00,0x08,0x08,0x08,0x08,0x08,0x00,0x60,0x60,0x00,0x00,0x20,0x10,0x08,0x04,0x02,0x3E,0x51,0x49,0x45,0x3E,0x00,0x42,0x7F,0x40,0x00,0x42,0x61,0x51,0x49,0x46,0x21,0x41,0x45,0x4B,0x31,0x18,0x14,0x12,0x7F,0x10,0x27,0x45,0x45,0x45,0x39,0x3C,0x4A,0x49,0x49,0x30,0x01,0x71,0x09,0x05,0x03,0x36,0x49,0x49,0x49,0x36,0x06,0x49,0x49,0x29,0x1E,0x00,0x36,0x36,0x00,0x00,0x00,0x56,0x36,0x00,0x00,0x00,0x08,0x14,0x22,0x41,0x14,0x14,0x14,0x14,0x14,0x41,0x22,0x14,0x08,0x00,0x02,0x01,0x51,0x09,0x06,0x32,0x49,0x79,0x41,0x3E,0x7E,0x11,0x11,0x11,0x7E,0x7F,0x49,0x49,0x49,0x36,0x3E,0x41,0x41,0x41,0x22,0x7F,0x41,0x41,0x22,0x1C,0x7F,0x49,0x49,0x49,0x41,0x7F,0x09,0x09,0x01,0x01,0x3E,0x41,0x41,0x51,0x32,0x7F,0x08,0x08,0x08,0x7F,0x00,0x41,0x7F,0x41,0x00,0x20,0x40,0x41,0x3F,0x01,0x7F,0x08,0x14,0x22,0x41,0x7F,0x40,0x40,0x40,0x40,0x7F,0x02,0x04,0x02,0x7F,0x7F,0x04,0x08,0x10,0x7F,0x3E,0x41,0x41,0x41,0x3E,0x7F,0x09,0x09,0x09,0x06,0x3E,0x41,0x51,0x21,0x5E,0x7F,0x09,0x19,0x29,0x46,0x46,0x49,0x49,0x49,0x31,0x01,0x01,0x7F,0x01,0x01,0x3F,0x40,0x40,0x40,0x3F,0x1F,0x20,0x40,0x20,0x1F,0x7F,0x20,0x18,0x20,0x7F,0x63,0x14,0x08,0x14,0x63,0x03,0x04,0x78,0x04,0x03,0x61,0x51,0x49,0x45,0x43,0x00,0x00,0x7F,0x41,0x41,0x02,0x04,0x08,0x10,0x20,0x41,0x41,0x7F,0x00,0x00,0x04,0x02,0x01,0x02,0x04,0x40,0x40,0x40,0x40,0x40,0x00,0x01,0x02,0x04,0x00,0x20,0x54,0x54,0x54,0x78,0x7F,0x48,0x44,0x44,0x38,0x38,0x44,0x44,0x44,0x20,0x38,0x44,0x44,0x48,0x7F,0x38,0x54,0x54,0x54,0x18,0x08,0x7E,0x09,0x01,0x02,0x08,0x14,0x54,0x54,0x3C,0x7F,0x08,0x04,0x04,0x78,0x00,0x44,0x7D,0x40,0x00,0x20,0x40,0x44,0x3D,0x00,0x00,0x7F,0x10,0x28,0x44,0x00,0x41,0x7F,0x40,0x00,0x7C,0x04,0x18,0x04,0x78,0x7C,0x08,0x04,0x04,0x78,0x38,0x44,0x44,0x44,0x38,0x7C,0x14,0x14,0x14,0x08,0x08,0x14,0x14,0x18,0x7C,0x7C,0x08,0x04,0x04,0x08,0x48,0x54,0x54,0x54,0x20,0x04,0x3F,0x44,0x40,0x20,0x3C,0x40,0x40,0x20,0x7C,0x1C,0x20,0x40,0x20,0x1C,0x3C,0x40,0x30,0x40,0x3C,0x44,0x28,0x10,0x28,0x44,0x0C,0x50,0x50,0x50,0x3C,0x44,0x64,0x54,0x4C,0x44,0x00,0x08,0x36,0x41,0x00,0x00,0x00,0x7F,0x00,0x00,0x00,0x41,0x36,0x08,0x00,0x02,0x01,0x02,0x04,0x02,0xff,0xff,0xff,0xff,0xff}
  function ShowChar()
   
    local x=0
    local tt=0
   for t=1,96 do
      tt=tt+1
      if tt==1 then 
         r=30 
         g=30
         b=1
      end
      if tt==2 then
         r=1
         g=30
         b=30
      end
      if tt==3 then
         r=30 
         g=1
         b=30
      end
      if tt==4 then
         r=30 
         g=1
         b=1
      end
      if tt==6 then
         r=1 
         g=1
         b=30
      end
      if tt==5 then
         r=1 
         g=30
         b=1
      end
      if tt==7 then
         r=30 
         g=30
         b=30
      end
     if tt>7 then tt=0 end
 
      for i=1 , 5 do -- 循环5次(5列)
        x=x+1
        line = lib_5X7[x]
         h=i
            for y = 1, 16 do
                 if h%2==0 then
                    AA = 0x01 << (16-y)
                  
                 else
                    AA = 0x01 << y-1
                 end
                  if line & AA ~= 0 then 
                   RGB[(y + (h-1)*16)*3-3]=r
                   RGB[(y + (h-1)*16)*3-2]=g
                   RGB[(y + (h-1)*16)*3-1]=b
                  else  
                   RGB[(y +(h-1)*16)*3-3]=1
                   RGB[(y +(h-1)*16)*3-2]=1
                   RGB[(y +(h-1)*16)*3-1]=1
                  end
                      
            end 
          
           
       end
       sys.wait(200)
       update()
       log.info("tag", t)
    end
end
sys.taskInit(function()
    while 1 do
        -- 仿呼吸灯效果
        ShowChar()      
       sys.wait(1000)
       
    end
end)

-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!
